(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@angular/core"), require("@angular/forms"), require("@angular/common"), require("rxjs/Subject"), require("rxjs/operator/debounceTime"), require("rxjs/operator/map"), require("@angular/material"));
	else if(typeof define === 'function' && define.amd)
		define(["@angular/core", "@angular/forms", "@angular/common", "rxjs/Subject", "rxjs/operator/debounceTime", "rxjs/operator/map", "@angular/material"], factory);
	else if(typeof exports === 'object')
		exports["ng-formly"] = factory(require("@angular/core"), require("@angular/forms"), require("@angular/common"), require("rxjs/Subject"), require("rxjs/operator/debounceTime"), require("rxjs/operator/map"), require("@angular/material"));
	else
		root["ng-formly"] = factory(root["@angular/core"], root["@angular/forms"], root["@angular/common"], root["rxjs/Subject"], root["rxjs/operator/debounceTime"], root["rxjs/operator/map"], root["@angular/material"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_6__, __WEBPACK_EXTERNAL_MODULE_7__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// CONCATENATED MODULE: ./node_modules/tslib/tslib.es6.js
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = Object.assign || function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
    }
    return t;
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);  }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { if (o[n]) i[n] = function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; }; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator];
    return m ? m.call(o) : typeof __values === "function" ? __values(o) : o[Symbol.iterator]();
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

// EXTERNAL MODULE: external "@angular/core"
var core_ = __webpack_require__(0);
var core__default = /*#__PURE__*/__webpack_require__.n(core_);

// EXTERNAL MODULE: external "@angular/forms"
var forms_ = __webpack_require__(1);
var forms__default = /*#__PURE__*/__webpack_require__.n(forms_);

// CONCATENATED MODULE: ./src/core/src/templates/field.ts



var field_Field = (function () {
    function Field() {
    }
    Object.defineProperty(Field.prototype, "key", {
        get: function () { return this.field.key; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "formControl", {
        get: function () { return this.field.formControl || this.form.get(this.key); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "templateOptions", {
        get: function () {
            console.warn(this.constructor.name + ": 'templateOptions' is deprecated. Use 'to' instead.");
            return this.to;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "to", {
        get: function () { return this.field.templateOptions; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "valid", {
        get: function () { return this.options.showError(this); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "id", {
        get: function () { return this.field.id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "formState", {
        get: function () { return this.options.formState || {}; },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", forms_["FormGroup"])
    ], Field.prototype, "form", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], Field.prototype, "field", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], Field.prototype, "model", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], Field.prototype, "options", void 0);
    return Field;
}());


// CONCATENATED MODULE: ./src/core/src/templates/field.type.ts


var field_type_FieldType = (function (_super) {
    __extends(FieldType, _super);
    function FieldType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FieldType.prototype.ngOnInit = function () {
        this.lifeCycleHooks(this.lifecycle.onInit);
    };
    FieldType.prototype.ngOnChanges = function (changes) {
        this.lifeCycleHooks(this.lifecycle.onChanges);
    };
    FieldType.prototype.ngDoCheck = function () {
        this.lifeCycleHooks(this.lifecycle.doCheck);
    };
    FieldType.prototype.ngAfterContentInit = function () {
        this.lifeCycleHooks(this.lifecycle.afterContentInit);
    };
    FieldType.prototype.ngAfterContentChecked = function () {
        this.lifeCycleHooks(this.lifecycle.afterContentChecked);
    };
    FieldType.prototype.ngAfterViewInit = function () {
        this.lifeCycleHooks(this.lifecycle.afterViewInit);
    };
    FieldType.prototype.ngAfterViewChecked = function () {
        this.lifeCycleHooks(this.lifecycle.afterViewChecked);
    };
    FieldType.prototype.ngOnDestroy = function () {
        this.lifeCycleHooks(this.lifecycle.onDestroy);
    };
    Object.defineProperty(FieldType.prototype, "lifecycle", {
        get: function () {
            return this.field.lifecycle || {};
        },
        enumerable: true,
        configurable: true
    });
    FieldType.prototype.lifeCycleHooks = function (callback) {
        if (callback) {
            callback.bind(this)(this.form, this.field, this.model, this.options);
        }
    };
    return FieldType;
}(field_Field));


// CONCATENATED MODULE: ./src/core/src/components/formly.group.ts



var formly_group_FormlyGroup = (function (_super) {
    __extends(FormlyGroup, _super);
    function FormlyGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormlyGroup.prototype, "newOptions", {
        get: function () {
            return __assign({}, this.options);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormlyGroup.prototype, "formlyGroup", {
        get: function () {
            if (this.field.formControl) {
                return this.field.formControl;
            }
            else {
                return this.form;
            }
        },
        enumerable: true,
        configurable: true
    });
    FormlyGroup = __decorate([
        Object(core_["Component"])({
            selector: 'formly-group',
            template: "\n    <formly-form [fields]=\"field.fieldGroup\" [model]=\"model\" [form]=\"formlyGroup\" [options]=\"newOptions\" [ngClass]=\"field.fieldGroupClassName\" [buildForm]=\"false\"></formly-form>\n  ",
        })
    ], FormlyGroup);
    return FormlyGroup;
}(field_type_FieldType));


// CONCATENATED MODULE: ./src/core/src/utils.ts
function getFieldId(formId, options, index) {
    if (options.id)
        return options.id;
    var type = options.type;
    if (!type && options.template)
        type = 'template';
    return [formId, type, options.key, index].join('_');
}
function getKeyPath(field) {
    if (field['_formlyKeyPath'] !== undefined) {
        return field['_formlyKeyPath'];
    }
    var keyPath = [];
    if (field.key) {
        var pathElements = typeof field.key === 'string' ? field.key.split('.') : field.key;
        for (var _i = 0, pathElements_1 = pathElements; _i < pathElements_1.length; _i++) {
            var pathElement = pathElements_1[_i];
            if (typeof pathElement === 'string') {
                pathElement = pathElement.replace(/\[(\w+)\]/g, '.$1');
                keyPath = keyPath.concat(pathElement.split('.'));
            }
            else {
                keyPath.push(pathElement);
            }
        }
        for (var i = 0; i < keyPath.length; i++) {
            var pathElement = keyPath[i];
            if (typeof pathElement === 'string' && stringIsInteger(pathElement)) {
                keyPath[i] = parseInt(pathElement);
            }
        }
    }
    field['_formlyKeyPath'] = keyPath;
    return keyPath;
}
function stringIsInteger(str) {
    return !isNullOrUndefined(str) && /^\d+$/.test(str);
}
function getFieldModel(model, field, constructEmptyObjects) {
    var keyPath = getKeyPath(field);
    var value = model;
    for (var i = 0; i < keyPath.length; i++) {
        var path = keyPath[i];
        var pathValue = value[path];
        if (isNullOrUndefined(pathValue) && constructEmptyObjects) {
            if (i < keyPath.length - 1) {
                value[path] = typeof keyPath[i + 1] === 'number' ? [] : {};
            }
            else if (field.fieldGroup) {
                value[path] = {};
            }
            else if (field.fieldArray) {
                value[path] = [];
            }
        }
        value = value[path];
        if (!value) {
            break;
        }
    }
    return value;
}
function assignModelValue(model, path, value) {
    if (typeof path === 'string') {
        path = getKeyPath({ key: path });
    }
    if (path.length > 1) {
        var e = path.shift();
        if (!model[e]) {
            model[e] = isNaN(path[0]) ? {} : [];
        }
        assignModelValue(model[e], path, value);
    }
    else {
        model[path[0]] = value;
    }
}
function getValueForKey(model, path) {
    if (typeof path === 'string') {
        path = getKeyPath({ key: path });
    }
    if (path.length > 1) {
        var e = path.shift();
        if (!model[e]) {
            model[e] = isNaN(path[0]) ? {} : [];
        }
        return getValueForKey(model[e], path);
    }
    else {
        return model[path[0]];
    }
}
function getKey(controlKey, actualKey) {
    return actualKey ? actualKey + '.' + controlKey : controlKey;
}
function reverseDeepMerge(dest, source) {
    if (source === void 0) { source = undefined; }
    var args = Array.prototype.slice.call(arguments);
    if (!args[1]) {
        return dest;
    }
    args.forEach(function (src, index) {
        if (!index) {
            return;
        }
        for (var srcArg in src) {
            if (isNullOrUndefined(dest[srcArg]) || isBlankString(dest[srcArg])) {
                if (isFunction(src[srcArg])) {
                    dest[srcArg] = src[srcArg];
                }
                else {
                    dest[srcArg] = clone(src[srcArg]);
                }
            }
            else if (objAndSameType(dest[srcArg], src[srcArg])) {
                reverseDeepMerge(dest[srcArg], src[srcArg]);
            }
        }
    });
    return dest;
}
function isNullOrUndefined(value) {
    return value === undefined || value === null;
}
function isUndefined(value) {
    return value === undefined;
}
function isBlankString(value) {
    return value === '';
}
function isFunction(value) {
    return typeof (value) === 'function';
}
function objAndSameType(obj1, obj2) {
    return isObject(obj1) && isObject(obj2) &&
        Object.getPrototypeOf(obj1) === Object.getPrototypeOf(obj2);
}
function isObject(x) {
    return x != null && typeof x === 'object';
}
function clone(value) {
    if (!isObject(value)) {
        return value;
    }
    return Array.isArray(value) ? value.slice(0) : Object.assign({}, value);
}
function evalStringExpression(expression, argNames) {
    try {
        return Function.bind.apply(Function, [void 0].concat(argNames.concat("return " + expression + ";")))();
    }
    catch (error) {
        console.error(error);
    }
}
function evalExpressionValueSetter(expression, argNames) {
    try {
        return Function.bind
            .apply(Function, [void 0].concat(argNames.concat(expression + " = expressionValue;")))();
    }
    catch (error) {
        console.error(error);
    }
}
function evalExpression(expression, thisArg, argVal) {
    if (expression instanceof Function) {
        return expression.apply(thisArg, argVal);
    }
    else {
        return expression ? true : false;
    }
}

// CONCATENATED MODULE: ./src/core/src/services/formly.config.ts




var FORMLY_CONFIG_TOKEN = new core_["OpaqueToken"]('FORMLY_CONFIG_TOKEN');
var formly_config_FormlyConfig = (function () {
    function FormlyConfig(configs) {
        if (configs === void 0) { configs = []; }
        var _this = this;
        this.types = {
            'formly-group': {
                name: 'formly-group',
                component: formly_group_FormlyGroup,
            },
        };
        this.validators = {};
        this.wrappers = {};
        this.templateManipulators = {
            preWrapper: [],
            postWrapper: [],
        };
        this.extras = {
            fieldTransform: undefined,
            showError: function (field) {
                return field.formControl.touched && !field.formControl.valid;
            },
        };
        configs.map(function (config) { return _this.addConfig(config); });
    }
    FormlyConfig.prototype.addConfig = function (config) {
        var _this = this;
        if (config.types) {
            config.types.map(function (type) { return _this.setType(type); });
        }
        if (config.validators) {
            config.validators.map(function (validator) { return _this.setValidator(validator); });
        }
        if (config.wrappers) {
            config.wrappers.map(function (wrapper) { return _this.setWrapper(wrapper); });
        }
        if (config.manipulators) {
            config.manipulators.map(function (manipulator) { return _this.setManipulator(manipulator); });
        }
        if (config.extras) {
            this.extras = __assign({}, this.extras, config.extras);
        }
    };
    FormlyConfig.prototype.setType = function (options) {
        var _this = this;
        if (Array.isArray(options)) {
            options.map(function (option) {
                _this.setType(option);
            });
        }
        else {
            if (!this.types[options.name]) {
                this.types[options.name] = {};
            }
            this.types[options.name].component = options.component;
            this.types[options.name].name = options.name;
            this.types[options.name].extends = options.extends;
            this.types[options.name].defaultOptions = options.defaultOptions;
            if (options.wrappers) {
                options.wrappers.map(function (wrapper) {
                    _this.setTypeWrapper(options.name, wrapper);
                });
            }
        }
    };
    FormlyConfig.prototype.getType = function (name) {
        if (!this.types[name]) {
            throw new Error("[Formly Error] There is no type by the name of \"" + name + "\"");
        }
        this.mergeExtendedType(name);
        return this.types[name];
    };
    FormlyConfig.prototype.getMergedField = function (field) {
        var _this = this;
        if (field === void 0) { field = {}; }
        var name = field.type;
        if (!this.types[name]) {
            throw new Error("[Formly Error] There is no type by the name of \"" + name + "\"");
        }
        this.mergeExtendedType(name);
        if (this.types[name].defaultOptions) {
            reverseDeepMerge(field, this.types[name].defaultOptions);
        }
        var extendDefaults = this.types[name].extends && this.getType(this.types[name].extends).defaultOptions;
        if (extendDefaults) {
            reverseDeepMerge(field, extendDefaults);
        }
        if (field && field.optionsTypes) {
            field.optionsTypes.map(function (option) {
                var defaultOptions = _this.getType(option).defaultOptions;
                if (defaultOptions) {
                    reverseDeepMerge(field, defaultOptions);
                }
            });
        }
        reverseDeepMerge(field, this.types[name]);
    };
    FormlyConfig.prototype.setWrapper = function (options) {
        var _this = this;
        this.wrappers[options.name] = options;
        if (options.types) {
            options.types.map(function (type) {
                _this.setTypeWrapper(type, options.name);
            });
        }
    };
    FormlyConfig.prototype.getWrapper = function (name) {
        if (!this.wrappers[name]) {
            throw new Error("[Formly Error] There is no wrapper by the name of \"" + name + "\"");
        }
        return this.wrappers[name];
    };
    FormlyConfig.prototype.setTypeWrapper = function (type, name) {
        if (!this.types[type]) {
            this.types[type] = {};
        }
        if (!this.types[type].wrappers) {
            this.types[type].wrappers = [];
        }
        this.types[type].wrappers.push(name);
    };
    FormlyConfig.prototype.setValidator = function (options) {
        this.validators[options.name] = options;
    };
    FormlyConfig.prototype.getValidator = function (name) {
        if (!this.validators[name]) {
            throw new Error("[Formly Error] There is no validator by the name of \"" + name + "\"");
        }
        return this.validators[name];
    };
    FormlyConfig.prototype.setManipulator = function (manipulator) {
        new manipulator.class()[manipulator.method](this);
    };
    FormlyConfig.prototype.mergeExtendedType = function (name) {
        if (!this.types[name].extends) {
            return;
        }
        var extendedType = this.getType(this.types[name].extends);
        if (!this.types[name].component) {
            this.types[name].component = extendedType.component;
        }
        if (!this.types[name].wrappers) {
            this.types[name].wrappers = extendedType.wrappers;
        }
    };
    FormlyConfig = __decorate([
        Object(core_["Injectable"])(),
        __param(0, Object(core_["Inject"])(FORMLY_CONFIG_TOKEN)),
        __metadata("design:paramtypes", [Array])
    ], FormlyConfig);
    return FormlyConfig;
}());


// CONCATENATED MODULE: ./src/core/src/services/formly.form.builder.ts






var formly_form_builder_FormlyFormBuilder = (function () {
    function FormlyFormBuilder(formlyConfig) {
        this.formlyConfig = formlyConfig;
        this.validationOpts = ['required', 'pattern', 'minLength', 'maxLength', 'min', 'max'];
        this.formId = 0;
    }
    FormlyFormBuilder.prototype.buildForm = function (form, fields, model, options) {
        if (fields === void 0) { fields = []; }
        this.formId++;
        options = options || {};
        if (!options.showError) {
            options.showError = this.formlyConfig.extras.showError;
        }
        var fieldTransforms = (options && options.fieldTransform) || this.formlyConfig.extras.fieldTransform;
        if (!Array.isArray(fieldTransforms)) {
            fieldTransforms = [fieldTransforms];
        }
        fieldTransforms.forEach(function (fieldTransform) {
            if (fieldTransform) {
                fields = fieldTransform(fields, model, form, options);
                if (!fields) {
                    throw new Error('fieldTransform must return an array of fields');
                }
            }
        });
        this.registerFormControls(form, fields, model, options);
    };
    FormlyFormBuilder.prototype.registerFormControls = function (form, fields, model, options) {
        var _this = this;
        fields.map(function (field, index) {
            field.id = getFieldId("formly_" + _this.formId, field, index);
            _this.initFieldTemplateOptions(field);
            _this.initFieldExpression(field, model, options);
            _this.initFieldValidation(field);
            _this.initFieldAsyncValidation(field);
            if (field.key && field.type) {
                _this.formlyConfig.getMergedField(field);
                var path = field.key;
                if (typeof path === 'string') {
                    if (!isUndefined(field.defaultValue)) {
                        _this.defaultPath = path;
                    }
                    path = getKeyPath({ key: field.key });
                }
                if (path.length > 1) {
                    var rootPath = path.shift();
                    var nestedForm = (form.get(rootPath.toString()) ? form.get(rootPath.toString()) : new forms_["FormGroup"]({}));
                    if (!form.get(rootPath.toString())) {
                        form.addControl(rootPath, nestedForm);
                    }
                    if (!model[rootPath]) {
                        model[rootPath] = isNaN(path[0]) ? {} : [];
                    }
                    var originalKey = field.key;
                    field.key = path;
                    _this.buildForm(nestedForm, [field], model[rootPath], options);
                    field.key = originalKey;
                }
                else {
                    if (!isUndefined(field.defaultValue) && isUndefined(model[path[0]])) {
                        var modelPath = getKeyPath({ key: _this.defaultPath });
                        modelPath = modelPath.pop();
                        assignModelValue(model, modelPath, field.defaultValue);
                        _this.defaultPath = undefined;
                    }
                    _this.addFormControl(form, field, model[path[0]]);
                }
            }
            if (field.fieldGroup) {
                if (field.key) {
                    if (!model.hasOwnProperty(field.key)) {
                        model[field.key] = {};
                    }
                    var nestedForm = form.get(field.key), nestedModel = model[field.key] || {};
                    if (!nestedForm) {
                        nestedForm = new forms_["FormGroup"]({}, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
                        _this.addControl(form, field.key, nestedForm, field);
                    }
                    _this.buildForm(nestedForm, field.fieldGroup, nestedModel, options);
                }
                else {
                    _this.buildForm(form, field.fieldGroup, model, options);
                }
            }
            if (field.fieldArray && field.key) {
                if (!(form.get(field.key) instanceof forms_["FormArray"])) {
                    var arrayForm = new forms_["FormArray"]([], field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
                    _this.addControl(form, field.key, arrayForm, field);
                }
            }
        });
    };
    FormlyFormBuilder.prototype.initFieldExpression = function (field, model, options) {
        options.formState = options.formState || {};
        if (field.expressionProperties) {
            for (var key in field.expressionProperties) {
                if (typeof field.expressionProperties[key] === 'string') {
                    field.expressionProperties[key] = {
                        expression: evalStringExpression(field.expressionProperties[key], ['model', 'formState']),
                        expressionValueSetter: evalExpressionValueSetter(key, ['expressionValue', 'model', 'templateOptions', 'validation']),
                    };
                }
                var expressionValue = evalExpression(field.expressionProperties[key].expression, { field: field }, [model, options.formState]);
                field.expressionProperties[key].expressionValue = expressionValue;
                evalExpression(field.expressionProperties[key].expressionValueSetter, { field: field }, [expressionValue, model, field.templateOptions || {}, field.validation]);
            }
        }
        if (typeof field.hideExpression === 'string') {
            field.hideExpression = evalStringExpression(field.hideExpression, ['model', 'formState']);
            field.hide = evalExpression(field.hideExpression, { field: field }, [model, options.formState]);
        }
    };
    FormlyFormBuilder.prototype.initFieldTemplateOptions = function (field) {
        if (field.key && field.type) {
            field.templateOptions = Object.assign({
                label: '',
                placeholder: '',
                focus: false,
            }, field.templateOptions);
        }
    };
    FormlyFormBuilder.prototype.initFieldAsyncValidation = function (field) {
        var _this = this;
        var validators = [];
        if (field.asyncValidators) {
            var _loop_1 = function (validatorName) {
                if (validatorName !== 'validation') {
                    validators.push(function (control) {
                        var validator = field.asyncValidators[validatorName];
                        if (isObject(validator)) {
                            validator = validator.expression;
                        }
                        return new Promise(function (resolve) {
                            return validator(control).then(function (result) {
                                resolve(result ? null : (_a = {}, _a[validatorName] = true, _a));
                                var _a;
                            });
                        });
                    });
                }
            };
            for (var validatorName in field.asyncValidators) {
                _loop_1(validatorName);
            }
        }
        if (field.asyncValidators && Array.isArray(field.asyncValidators.validation)) {
            field.asyncValidators.validation.map(function (validate) {
                if (typeof validate === 'string') {
                    validators.push(_this.formlyConfig.getValidator(validate).validation);
                }
                else {
                    validators.push(validate);
                }
            });
        }
        if (validators.length) {
            if (field.asyncValidators && !Array.isArray(field.asyncValidators.validation)) {
                field.asyncValidators.validation = forms_["Validators"].composeAsync([field.asyncValidators.validation].concat(validators));
            }
            else {
                field.asyncValidators = {
                    validation: forms_["Validators"].composeAsync(validators),
                };
            }
        }
    };
    FormlyFormBuilder.prototype.initFieldValidation = function (field) {
        var _this = this;
        var validators = [];
        this.validationOpts.filter(function (opt) { return field.templateOptions && field.templateOptions[opt]; }).map(function (opt) {
            validators.push(_this.getValidation(opt, field.templateOptions[opt]));
        });
        if (field.validators) {
            var _loop_2 = function (validatorName) {
                if (validatorName !== 'validation') {
                    validators.push(function (control) {
                        var validator = field.validators[validatorName];
                        if (isObject(validator)) {
                            validator = validator.expression;
                        }
                        return validator(control) ? null : (_a = {}, _a[validatorName] = true, _a);
                        var _a;
                    });
                }
            };
            for (var validatorName in field.validators) {
                _loop_2(validatorName);
            }
        }
        if (field.validators && Array.isArray(field.validators.validation)) {
            field.validators.validation.map(function (validate) {
                if (typeof validate === 'string') {
                    validators.push(_this.formlyConfig.getValidator(validate).validation);
                }
                else {
                    validators.push(validate);
                }
            });
        }
        if (validators.length) {
            if (field.validators && !Array.isArray(field.validators.validation)) {
                field.validators.validation = forms_["Validators"].compose([field.validators.validation].concat(validators));
            }
            else {
                field.validators = {
                    validation: forms_["Validators"].compose(validators),
                };
            }
        }
    };
    FormlyFormBuilder.prototype.addFormControl = function (form, field, model) {
        var name = typeof field.key === 'string' ? field.key : field.key[0], formControl;
        if (field.formControl instanceof forms_["AbstractControl"]) {
            formControl = field.formControl;
        }
        else if (field.component && field.component.createControl) {
            formControl = field.component.createControl(model, field);
        }
        else {
            formControl = new forms_["FormControl"](model, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
        }
        if (field.templateOptions.disabled) {
            formControl.disable();
        }
        this.addControl(form, name, formControl, field);
        if (field.validation && field.validation.show) {
            form.get(field.key).markAsTouched();
        }
    };
    FormlyFormBuilder.prototype.getValidation = function (opt, value) {
        var _this = this;
        switch (opt) {
            case 'required':
                return forms_["Validators"].required;
            case 'pattern':
                return forms_["Validators"].pattern(value);
            case 'minLength':
                return forms_["Validators"].minLength(value);
            case 'maxLength':
                return forms_["Validators"].maxLength(value);
            case 'min':
            case 'max':
                return function (changes) {
                    if (_this.checkMinMax(opt, changes.value, value)) {
                        return null;
                    }
                    else {
                        return _a = {}, _a[opt] = true, _a;
                    }
                    var _a;
                };
        }
    };
    FormlyFormBuilder.prototype.checkMinMax = function (opt, changes, value) {
        if (changes == null || changes === '') {
            return true;
        }
        if (opt === 'min') {
            return parseInt(changes) >= value;
        }
        return parseInt(changes) <= value;
    };
    FormlyFormBuilder.prototype.addControl = function (form, key, formControl, field) {
        field.formControl = formControl;
        if (field.hide) {
            return;
        }
        if (formControl instanceof forms_["FormArray"]) {
            form.setControl(key, formControl);
        }
        else {
            form.addControl(key, formControl);
        }
    };
    FormlyFormBuilder = __decorate([
        Object(core_["Injectable"])(),
        __metadata("design:paramtypes", [formly_config_FormlyConfig])
    ], FormlyFormBuilder);
    return FormlyFormBuilder;
}());


// CONCATENATED MODULE: ./src/core/src/components/formly.form.ts





var formly_form_FormlyForm = (function () {
    function FormlyForm(formlyBuilder) {
        this.formlyBuilder = formlyBuilder;
        this.model = {};
        this.form = new forms_["FormGroup"]({});
        this.fields = [];
        this.buildForm = true;
    }
    FormlyForm.prototype.ngOnChanges = function (changes) {
        if (changes['fields']) {
            this.model = this.model || {};
            this.form = this.form || (new forms_["FormGroup"]({}));
            this.setOptions();
            if (this.buildForm !== false) {
                this.formlyBuilder.buildForm(this.form, this.fields, this.model, this.options);
            }
            this.updateInitialValue();
        }
        else if (changes['model'] && this.fields && this.fields.length > 0) {
            this.form.patchValue(this.model);
        }
    };
    FormlyForm.prototype.fieldModel = function (field) {
        if (field.key && (field.fieldGroup || field.fieldArray)) {
            return getFieldModel(this.model, field, true);
        }
        return this.model;
    };
    FormlyForm.prototype.changeModel = function (event) {
        assignModelValue(this.model, event.key, event.value);
    };
    FormlyForm.prototype.setOptions = function () {
        this.options = this.options || {};
        if (!this.options.resetModel) {
            this.options.resetModel = this.resetModel.bind(this);
        }
        if (!this.options.updateInitialValue) {
            this.options.updateInitialValue = this.updateInitialValue.bind(this);
        }
    };
    FormlyForm.prototype.resetModel = function (model) {
        model = isNullOrUndefined(model) ? this.initialModel : model;
        this.form.patchValue(model);
        this.resetFormGroup(model, this.form);
        this.resetFormModel(model, this.model);
    };
    FormlyForm.prototype.resetFormModel = function (model, formModel, path) {
        if (!isObject(model) && !Array.isArray(model)) {
            return;
        }
        for (var key in formModel) {
            if (!(key in model) || isNullOrUndefined(model[key])) {
                if (!this.form.get((path || []).concat(key))) {
                    delete formModel[key];
                }
            }
        }
        for (var key in model) {
            if (!isNullOrUndefined(model[key])) {
                if (key in formModel) {
                    this.resetFormModel(model[key], formModel[key], (path || []).concat(key));
                }
                else {
                    formModel[key] = model[key];
                }
            }
        }
    };
    FormlyForm.prototype.resetFormGroup = function (model, form, actualKey) {
        for (var controlKey in form.controls) {
            var key = getKey(controlKey, actualKey);
            if (form.controls[controlKey] instanceof forms_["FormGroup"]) {
                this.resetFormGroup(model, form.controls[controlKey], key);
            }
            if (form.controls[controlKey] instanceof forms_["FormArray"]) {
                this.resetFormArray(model, form.controls[controlKey], key);
            }
            if (form.controls[controlKey] instanceof forms_["FormControl"]) {
                form.controls[controlKey].setValue(getValueForKey(model, key));
            }
        }
    };
    FormlyForm.prototype.resetFormArray = function (model, formArray, key) {
        var newValue = getValueForKey(model, key);
        for (var i = formArray.length - 1; i >= 0; i--) {
            if (formArray.at(i) instanceof forms_["FormGroup"]) {
                if (newValue && !isNullOrUndefined(newValue[i])) {
                    this.resetFormGroup(newValue[i], formArray.at(i));
                }
                else {
                    formArray.removeAt(i);
                    var value = getValueForKey(this.model, key);
                    if (Array.isArray(value)) {
                        value.splice(i, 1);
                    }
                }
            }
        }
        if (Array.isArray(newValue) && formArray.length < newValue.length) {
            var remaining = newValue.length - formArray.length;
            var initialLength = formArray.length;
            for (var i = 0; i < remaining; i++) {
                var pos = initialLength + i;
                getValueForKey(this.model, key).push(newValue[pos]);
                formArray.push(new forms_["FormGroup"]({}));
            }
        }
    };
    FormlyForm.prototype.updateInitialValue = function () {
        var obj = reverseDeepMerge(this.form.value, this.model);
        this.initialModel = JSON.parse(JSON.stringify(obj));
    };
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "model", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", forms_["FormGroup"])
    ], FormlyForm.prototype, "form", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Array)
    ], FormlyForm.prototype, "fields", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "options", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Boolean)
    ], FormlyForm.prototype, "buildForm", void 0);
    FormlyForm = __decorate([
        Object(core_["Component"])({
            selector: 'formly-form',
            template: "\n    <formly-field *ngFor=\"let field of fields\"\n      [model]=\"fieldModel(field)\" [form]=\"form\"\n      [field]=\"field\" (modelChange)=\"changeModel($event)\"\n      [ngClass]=\"field.className\"\n      [options]=\"options\">\n    </formly-field>\n    <ng-content></ng-content>\n  ",
        }),
        __metadata("design:paramtypes", [formly_form_builder_FormlyFormBuilder])
    ], FormlyForm);
    return FormlyForm;
}());


// EXTERNAL MODULE: external "rxjs/Subject"
var Subject_ = __webpack_require__(4);
var Subject__default = /*#__PURE__*/__webpack_require__.n(Subject_);

// CONCATENATED MODULE: ./src/core/src/services/formly.event.emitter.ts


var FormlyValueChangeEvent = (function () {
    function FormlyValueChangeEvent(key, value) {
        this.key = key;
        this.value = value;
    }
    return FormlyValueChangeEvent;
}());

var formly_event_emitter_FormlyEventEmitter = (function (_super) {
    __extends(FormlyEventEmitter, _super);
    function FormlyEventEmitter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyEventEmitter.prototype.emit = function (value) {
        _super.prototype.next.call(this, value);
    };
    return FormlyEventEmitter;
}(Subject_["Subject"]));

var FormlyPubSub = (function () {
    function FormlyPubSub() {
        this.emitters = {};
    }
    FormlyPubSub.prototype.setEmitter = function (key, emitter) {
        this.emitters[key] = emitter;
    };
    FormlyPubSub.prototype.getEmitter = function (key) {
        return this.emitters[key];
    };
    FormlyPubSub.prototype.removeEmitter = function (key) {
        delete this.emitters[key];
    };
    return FormlyPubSub;
}());


// EXTERNAL MODULE: external "rxjs/operator/debounceTime"
var debounceTime_ = __webpack_require__(5);
var debounceTime__default = /*#__PURE__*/__webpack_require__.n(debounceTime_);

// EXTERNAL MODULE: external "rxjs/operator/map"
var map_ = __webpack_require__(6);
var map__default = /*#__PURE__*/__webpack_require__.n(map_);

// CONCATENATED MODULE: ./src/core/src/components/formly.field.ts








var formly_field_FormlyField = (function () {
    function FormlyField(elementRef, formlyPubSub, renderer, formlyConfig, componentFactoryResolver) {
        this.elementRef = elementRef;
        this.formlyPubSub = formlyPubSub;
        this.renderer = renderer;
        this.formlyConfig = formlyConfig;
        this.componentFactoryResolver = componentFactoryResolver;
        this.options = {};
        this.modelChange = new core_["EventEmitter"]();
        this.componentRefs = [];
        this._subscriptions = [];
    }
    FormlyField.prototype.ngDoCheck = function () {
        this.checkExpressionChange();
        this.checkVisibilityChange();
    };
    FormlyField.prototype.ngOnInit = function () {
        this.createFieldComponents();
        if (this.field.hide === true) {
            this.toggleHide(true);
        }
    };
    FormlyField.prototype.ngOnDestroy = function () {
        this.componentRefs.map(function (componentRef) { return componentRef.destroy(); });
        this._subscriptions.map(function (subscriber) { return subscriber.unsubscribe(); });
        this._subscriptions = this.componentRefs = [];
        if (this.field && this.field.key) {
            this.formlyPubSub.removeEmitter(this.field.key);
        }
    };
    FormlyField.prototype.changeModel = function (event) {
        this.modelChange.emit(event);
    };
    FormlyField.prototype.createFieldComponents = function () {
        var _this = this;
        if (this.field && !this.field.template && !this.field.fieldGroup && !this.field.fieldArray) {
            var debounce = 0;
            if (this.field.modelOptions && this.field.modelOptions.debounce && this.field.modelOptions.debounce.default) {
                debounce = this.field.modelOptions.debounce.default;
            }
            var fieldComponentRef = this.createFieldComponent();
            if (this.field.key) {
                var valueChanges_1 = fieldComponentRef.instance.formControl.valueChanges;
                if (debounce > 0) {
                    valueChanges_1 = debounceTime_["debounceTime"].call(valueChanges_1, debounce);
                }
                if (this.field.parsers && this.field.parsers.length > 0) {
                    this.field.parsers.map(function (parserFn) {
                        valueChanges_1 = map_["map"].call(valueChanges_1, parserFn);
                    });
                }
                this._subscriptions.push(valueChanges_1.subscribe(function (event) { return _this
                    .changeModel(new FormlyValueChangeEvent(_this.field.key, event)); }));
            }
            var update = new formly_event_emitter_FormlyEventEmitter();
            this._subscriptions.push(update.subscribe(function (option) {
                _this.field.templateOptions[option.key] = option.value;
            }));
            this.formlyPubSub.setEmitter(this.field.key, update);
        }
        else if (this.field.fieldGroup || this.field.fieldArray) {
            this.createFieldComponent();
        }
    };
    FormlyField.prototype.createFieldComponent = function () {
        var _this = this;
        if (this.field.fieldGroup) {
            this.field.type = this.field.type || 'formly-group';
        }
        var type = this.formlyConfig.getType(this.field.type), wrappers = this.getFieldWrappers(type);
        var fieldComponent = this.fieldComponent;
        wrappers.map(function (wrapperName) {
            var wrapperRef = _this.createComponent(fieldComponent, _this.formlyConfig.getWrapper(wrapperName).component);
            fieldComponent = wrapperRef.instance.fieldComponent;
        });
        return this.createComponent(fieldComponent, type.component);
    };
    FormlyField.prototype.getFieldWrappers = function (type) {
        var _this = this;
        var templateManipulators = {
            preWrapper: [],
            postWrapper: [],
        };
        if (this.field.templateOptions) {
            this.mergeTemplateManipulators(templateManipulators, this.field.templateOptions.templateManipulators);
        }
        this.mergeTemplateManipulators(templateManipulators, this.formlyConfig.templateManipulators);
        var preWrappers = templateManipulators.preWrapper.map(function (m) { return m(_this.field); }).filter(function (type) { return type; }), postWrappers = templateManipulators.postWrapper.map(function (m) { return m(_this.field); }).filter(function (type) { return type; });
        if (!this.field.wrappers)
            this.field.wrappers = [];
        if (!type.wrappers)
            type.wrappers = [];
        if (this.field.wrapper) {
            console.warn(this.field.key + ": wrapper is deprecated. Use 'wrappers' instead.");
            this.field.wrappers = Array.isArray(this.field.wrapper) ? this.field.wrapper : [this.field.wrapper];
        }
        return preWrappers.concat(this.field.wrappers, postWrappers);
    };
    FormlyField.prototype.mergeTemplateManipulators = function (source, target) {
        target = target || {};
        if (target.preWrapper) {
            source.preWrapper = source.preWrapper.concat(target.preWrapper);
        }
        if (target.postWrapper) {
            source.postWrapper = source.postWrapper.concat(target.postWrapper);
        }
        return source;
    };
    FormlyField.prototype.createComponent = function (fieldComponent, component) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
        var ref = fieldComponent.createComponent(componentFactory);
        Object.assign(ref.instance, {
            model: this.model,
            form: this.form,
            field: this.field,
            options: this.options,
        });
        this.componentRefs.push(ref);
        return ref;
    };
    FormlyField.prototype.psEmit = function (fieldKey, eventKey, value) {
        if (this.formlyPubSub && this.formlyPubSub.getEmitter(fieldKey) && this.formlyPubSub.getEmitter(fieldKey).emit) {
            this.formlyPubSub.getEmitter(fieldKey).emit(new FormlyValueChangeEvent(eventKey, value));
        }
    };
    FormlyField.prototype.checkVisibilityChange = function () {
        if (this.field && this.field.hideExpression) {
            var hideExpressionResult = !!evalExpression(this.field.hideExpression, this, [this.model, this.options.formState]);
            if (hideExpressionResult !== this.field.hide) {
                this.toggleHide(hideExpressionResult);
            }
        }
    };
    FormlyField.prototype.checkExpressionChange = function () {
        if (this.field && this.field.expressionProperties) {
            var expressionProperties = this.field.expressionProperties;
            for (var key in expressionProperties) {
                var expressionValue = evalExpression(expressionProperties[key].expression, this, [this.model, this.options.formState]);
                if (expressionProperties[key].expressionValue !== expressionValue) {
                    expressionProperties[key].expressionValue = expressionValue;
                    evalExpression(expressionProperties[key].expressionValueSetter, this, [expressionValue, this.model, this.field.templateOptions, this.field.validation]);
                }
            }
            var formControl = this.field.formControl;
            if (formControl) {
                if (formControl.status === 'DISABLED' && !this.field.templateOptions.disabled) {
                    formControl.enable();
                }
                if (formControl.status !== 'DISABLED' && this.field.templateOptions.disabled) {
                    formControl.disable();
                }
                if (!formControl.dirty && formControl.invalid && this.field.validation && !this.field.validation.show) {
                    formControl.markAsUntouched();
                }
                if (!formControl.dirty && formControl.invalid && this.field.validation && this.field.validation.show) {
                    formControl.markAsTouched();
                }
            }
        }
    };
    FormlyField.prototype.toggleHide = function (value) {
        var _this = this;
        this.field.hide = value;
        if (this.field.formControl) {
            if (value === true && this.form.get(this.field.key)) {
                setTimeout(function () { return _this.removeFieldControl(); });
            }
            else if (value === false && !this.form.get(this.field.key)) {
                setTimeout(function () { return _this.addFieldControl(); });
            }
        }
        this.renderer.setElementStyle(this.elementRef.nativeElement, 'display', value ? 'none' : '');
        if (this.field.fieldGroup) {
            for (var i = 0; i < this.field.fieldGroup.length; i++) {
                this.psEmit(this.field.fieldGroup[i].key, 'hidden', value);
            }
        }
        else {
            this.psEmit(this.field.key, 'hidden', value);
        }
    };
    Object.defineProperty(FormlyField.prototype, "fieldKey", {
        get: function () {
            return this.field.key.split('.').pop();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormlyField.prototype, "fieldParentFormControl", {
        get: function () {
            var paths = this.field.key.split('.');
            paths.pop();
            return (paths.length > 0 ? this.form.get(paths) : this.form);
        },
        enumerable: true,
        configurable: true
    });
    FormlyField.prototype.addFieldControl = function () {
        var parent = this.fieldParentFormControl;
        if (parent instanceof forms_["FormArray"]) {
            parent.push(this.field.formControl);
        }
        else if (parent instanceof forms_["FormGroup"]) {
            parent.addControl(this.fieldKey, this.field.formControl);
        }
    };
    FormlyField.prototype.removeFieldControl = function () {
        var parent = this.fieldParentFormControl;
        if (parent instanceof forms_["FormArray"]) {
            parent.removeAt(this.fieldKey);
        }
        else if (parent instanceof forms_["FormGroup"]) {
            parent.removeControl(this.fieldKey);
        }
    };
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "model", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", forms_["FormGroup"])
    ], FormlyField.prototype, "form", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "field", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "options", void 0);
    __decorate([
        Object(core_["Output"])(),
        __metadata("design:type", core_["EventEmitter"])
    ], FormlyField.prototype, "modelChange", void 0);
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyField.prototype, "fieldComponent", void 0);
    FormlyField = __decorate([
        Object(core_["Component"])({
            selector: 'formly-field',
            template: "\n    <ng-container #fieldComponent></ng-container>\n    <div *ngIf=\"field.template && !field.fieldGroup\" [innerHtml]=\"field.template\"></div>\n  ",
        }),
        __metadata("design:paramtypes", [core_["ElementRef"],
            FormlyPubSub,
            core_["Renderer"],
            formly_config_FormlyConfig,
            core_["ComponentFactoryResolver"]])
    ], FormlyField);
    return FormlyField;
}());


// CONCATENATED MODULE: ./src/core/src/components/formly.attributes.ts



var formly_attributes_FormlyAttributes = (function () {
    function FormlyAttributes(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.attributes = ['id', 'name', 'placeholder', 'tabindex', 'step', 'aria-describedby'];
        this.statements = ['change', 'keydown', 'keyup', 'keypress', 'click', 'focus', 'blur'];
    }
    FormlyAttributes.prototype.onFocus = function () {
        this.field.focus = true;
    };
    FormlyAttributes.prototype.onBlur = function () {
        this.field.focus = false;
    };
    FormlyAttributes.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['field']) {
            var fieldChanges_1 = changes['field'];
            this.attributes
                .filter(function (attr) { return _this.canApplyRender(fieldChanges_1, attr); })
                .map(function (attr) { return _this.renderer.setElementAttribute(_this.elementRef.nativeElement, attr, _this.getPropValue(_this.field, attr)); });
            this.statements
                .filter(function (statement) { return _this.canApplyRender(fieldChanges_1, statement); })
                .map(function (statement) { return _this.renderer.listen(_this.elementRef.nativeElement, statement, _this.getStatementValue(statement)); });
            if ((fieldChanges_1.previousValue || {}).focus !== (fieldChanges_1.currentValue || {}).focus) {
                this.renderer.invokeElementMethod(this.elementRef.nativeElement, this.field.focus ? 'focus' : 'blur', []);
            }
        }
    };
    FormlyAttributes.prototype.getPropValue = function (field, prop) {
        field = field || {};
        if (field.id && prop === 'aria-describedby') {
            return field.id + '-message';
        }
        if (field.templateOptions && field.templateOptions[prop]) {
            return field.templateOptions[prop];
        }
        return field[prop];
    };
    FormlyAttributes.prototype.getStatementValue = function (statement) {
        var _this = this;
        var fn = this.field.templateOptions[statement];
        return function () { return fn(_this.field, _this.formControl); };
    };
    FormlyAttributes.prototype.canApplyRender = function (fieldChange, prop) {
        var currentValue = this.getPropValue(this.field, prop), previousValue = this.getPropValue(fieldChange.previousValue, prop);
        if (previousValue !== currentValue) {
            if (this.statements.indexOf(prop) !== -1) {
                return typeof currentValue === 'function';
            }
            return true;
        }
        return false;
    };
    __decorate([
        Object(core_["Input"])('formlyAttributes'),
        __metadata("design:type", Object)
    ], FormlyAttributes.prototype, "field", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", forms_["AbstractControl"])
    ], FormlyAttributes.prototype, "formControl", void 0);
    __decorate([
        Object(core_["HostListener"])('focus'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FormlyAttributes.prototype, "onFocus", null);
    __decorate([
        Object(core_["HostListener"])('blur'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FormlyAttributes.prototype, "onBlur", null);
    FormlyAttributes = __decorate([
        Object(core_["Directive"])({
            selector: '[formlyAttributes]',
        }),
        __metadata("design:paramtypes", [core_["Renderer"],
            core_["ElementRef"]])
    ], FormlyAttributes);
    return FormlyAttributes;
}());


// CONCATENATED MODULE: ./src/core/src/services/formly.validation-messages.ts



var formly_validation_messages_FormlyValidationMessages = (function () {
    function FormlyValidationMessages(configs) {
        if (configs === void 0) { configs = []; }
        var _this = this;
        this.messages = {};
        configs.map(function (config) {
            if (config.validationMessages) {
                config.validationMessages.map(function (validation) { return _this.addStringMessage(validation.name, validation.message); });
            }
        });
    }
    FormlyValidationMessages.prototype.addStringMessage = function (name, message) {
        this.messages[name] = message;
    };
    FormlyValidationMessages.prototype.getMessages = function () {
        return this.messages;
    };
    FormlyValidationMessages.prototype.getValidatorErrorMessage = function (name) {
        return this.messages[name];
    };
    FormlyValidationMessages = __decorate([
        Object(core_["Injectable"])(),
        __param(0, Object(core_["Inject"])(FORMLY_CONFIG_TOKEN)),
        __metadata("design:paramtypes", [Array])
    ], FormlyValidationMessages);
    return FormlyValidationMessages;
}());


// CONCATENATED MODULE: ./src/core/src/templates/field.wrapper.ts


var field_wrapper_FieldWrapper = (function (_super) {
    __extends(FieldWrapper, _super);
    function FieldWrapper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return FieldWrapper;
}(field_Field));


// EXTERNAL MODULE: external "@angular/common"
var common_ = __webpack_require__(2);
var common__default = /*#__PURE__*/__webpack_require__.n(common_);

// CONCATENATED MODULE: ./src/core/src/core.module.ts












var FORMLY_DIRECTIVES = [formly_form_FormlyForm, formly_field_FormlyField, formly_attributes_FormlyAttributes, formly_group_FormlyGroup];
var core_module_FormlyModule = (function () {
    function FormlyModule() {
    }
    FormlyModule_1 = FormlyModule;
    FormlyModule.forRoot = function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: FormlyModule_1,
            providers: [
                formly_form_builder_FormlyFormBuilder,
                formly_config_FormlyConfig,
                FormlyPubSub,
                formly_validation_messages_FormlyValidationMessages,
                { provide: FORMLY_CONFIG_TOKEN, useValue: config, multi: true },
                { provide: core_["ANALYZE_FOR_ENTRY_COMPONENTS"], useValue: config, multi: true },
            ],
        };
    };
    FormlyModule.forChild = function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: FormlyModule_1,
            providers: [
                { provide: FORMLY_CONFIG_TOKEN, useValue: config, multi: true },
                { provide: core_["ANALYZE_FOR_ENTRY_COMPONENTS"], useValue: config, multi: true },
            ],
        };
    };
    FormlyModule = FormlyModule_1 = __decorate([
        Object(core_["NgModule"])({
            declarations: FORMLY_DIRECTIVES,
            entryComponents: [formly_group_FormlyGroup],
            exports: FORMLY_DIRECTIVES,
            imports: [
                common_["CommonModule"],
                forms_["ReactiveFormsModule"],
            ],
        })
    ], FormlyModule);
    return FormlyModule;
    var FormlyModule_1;
}());


// CONCATENATED MODULE: ./src/core/src/core.ts












// CONCATENATED MODULE: ./src/core/public_api.ts


// CONCATENATED MODULE: ./src/core/index.ts


// CONCATENATED MODULE: ./src/material/src/types/checkbox.ts




var checkbox_FormlyFieldCheckbox = (function (_super) {
    __extends(FormlyFieldCheckbox, _super);
    function FormlyFieldCheckbox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldCheckbox.createControl = function (model, field) {
        return new forms_["FormControl"]({ value: model ? 'on' : undefined, disabled: field.templateOptions.disabled }, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
    };
    FormlyFieldCheckbox = __decorate([
        Object(core_["Component"])({
            selector: "formly-field-checkbox",
            template: "\n  <div class=\"form-group\">\n  <div [formGroup]=\"form\">\n    <label class=\"c-input c-checkbox\">\n      <md-checkbox (change)=\"ngOnChanges($event)\" [formControlName]=\"key\" ng-model=\"model\"\n      [disabled]=\"to.disabled\" (focus)=\"onInputFocus()\" #inputElement> {{to.label}}\n      </md-checkbox>\n      </label>\n  </div>\n",
            queries: { inputComponent: new core_["ViewChildren"]("inputElement") }
        })
    ], FormlyFieldCheckbox);
    return FormlyFieldCheckbox;
}(field_type_FieldType));


// CONCATENATED MODULE: ./src/material/src/types/multicheckbox.ts




var multicheckbox_FormlyFieldMultiCheckbox = (function (_super) {
    __extends(FormlyFieldMultiCheckbox, _super);
    function FormlyFieldMultiCheckbox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldMultiCheckbox.createControl = function (model, field) {
        return new forms_["FormControl"]({ value: model ? 'on' : undefined, disabled: field.templateOptions.disabled }, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
    };
    FormlyFieldMultiCheckbox = __decorate([
        Object(core_["Component"])({
            selector: "formly-field-multicheckbox",
            template: "\n  <div [formGroup]=\"form\">\n  <div [formGroupName]=\"key\" class=\"form-group\">\n      <label class=\"form-control-label\" for=\"\">{{to.label}}</label>\n      <div *ngFor=\"let option of to.options\">\n          <label class=\"c-input c-radio\">\n              <md-checkbox type=\"checkbox\" [formControlName]=\"option.key\"\n                ng-model=\"model[option.key]\" (change)=\"ngOnChanges($event, option.key)\"\n                (focus)=\"onInputFocus()\" [disabled]=\"to.disabled\" #inputElement>{{option.value}}\n              </md-checkbox>\n              <span class=\"c-indicator\"></span>\n          </label>\n      </div>\n  <!--    <small class=\"text-muted\">{{to.description}}</small> -->\n  </div>\n</div>\n",
            queries: { inputComponent: new core_["ViewChildren"]("inputElement") }
        })
    ], FormlyFieldMultiCheckbox);
    return FormlyFieldMultiCheckbox;
}(field_type_FieldType));


// CONCATENATED MODULE: ./src/material/src/types/input.ts




var input_FormlyFieldInput = (function (_super) {
    __extends(FormlyFieldInput, _super);
    function FormlyFieldInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldInput.createControl = function (model, field) {
        return new forms_["FormControl"]({ value: model ? 'on' : undefined, disabled: field.templateOptions.disabled }, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
    };
    FormlyFieldInput = __decorate([
        Object(core_["Component"])({
            selector: "formly-field-checkbox",
            template: "\n  <div class=\"formly-md-input-wrapper\" [formGroup]=\"form\" [ngClass]=\"{'has-danger': !formControl.valid}\" *ngIf=\"!to.hidden\">\n  <md-input-container>\n  <input mdInput [type]=\"to.type\" [formControlName]=\"key\" class=\"form-control md-input-full-width\" id=\"{{key}}\"\n    [disabled]=\"to.disabled\"\n    (keyup)=\"inputChange($event, 'value')\" (change)=\"inputChange($event, 'value')\" [(ngModel)]=\"model\"\n    (focus)=\"onInputFocus()\" [ngClass]=\"{'form-control-danger': !form.controls[key].valid}\" #inputElement>\n    <md-placeholder [ngClass]=\"{'md-placeholder-error': !formControl.valid}\">{{to.label}}</md-placeholder>\n    </md-input-container>\n  <small class=\"md-hint\">{{to.description}}</small>\n</div>\n",
            queries: { inputComponent: new core_["ViewChildren"]("inputElement") }
        })
    ], FormlyFieldInput);
    return FormlyFieldInput;
}(field_type_FieldType));


// CONCATENATED MODULE: ./src/material/src/types/radio.ts




var radio_FormlyFieldRadio = (function (_super) {
    __extends(FormlyFieldRadio, _super);
    function FormlyFieldRadio() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldRadio.createControl = function (model, field) {
        return new forms_["FormControl"]({ value: model ? 'on' : undefined, disabled: field.templateOptions.disabled }, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
    };
    FormlyFieldRadio = __decorate([
        Object(core_["Component"])({
            selector: "formly-field-radio",
            template: "\n  <div [formGroup]=\"form\">\n  <label for=\"\">{{to.label}}</label>\n  <md-radio-group [(ngModel)]=\"model\" [formControlName]=\"key\">\n    <p *ngFor=\"let option of to.options\">\n      <md-radio-button [value]=\"option.key\" \n        (change)=\"inputChange($event, option.key)\" (focus)=\"onInputFocus()\"\n        [disabled]=\"to.disabled\" #inputElement>{{option.value}}\n      </md-radio-button>\n    </p>\n  </md-radio-group>\n</div>\n    ",
            queries: { inputComponent: new core_["ViewChildren"]("inputElement") }
        })
    ], FormlyFieldRadio);
    return FormlyFieldRadio;
}(field_type_FieldType));


// CONCATENATED MODULE: ./src/material/src/types/types.ts





// CONCATENATED MODULE: ./src/material/src/wrappers/fieldset.ts



var fieldset_FormlyWrapperFieldset = (function (_super) {
    __extends(FormlyWrapperFieldset, _super);
    function FormlyWrapperFieldset() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyWrapperFieldset.prototype, "fieldComponent", void 0);
    FormlyWrapperFieldset = __decorate([
        Object(core_["Component"])({
            selector: 'formly-wrapper-fieldset',
            template: "\n    <div class=\"form-group\" [ngClass]=\"{'has-danger': valid}\">\n      <ng-container #fieldComponent></ng-container>\n    </div>\n  ",
        })
    ], FormlyWrapperFieldset);
    return FormlyWrapperFieldset;
}(field_wrapper_FieldWrapper));


// CONCATENATED MODULE: ./src/material/src/wrappers/label.ts



var label_FormlyWrapperLabel = (function (_super) {
    __extends(FormlyWrapperLabel, _super);
    function FormlyWrapperLabel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyWrapperLabel.prototype, "fieldComponent", void 0);
    FormlyWrapperLabel = __decorate([
        Object(core_["Component"])({
            selector: 'formly-wrapper-label',
            template: "\n    <label [attr.for]=\"id\" class=\"form-control-label\">{{ to.label }}</label>\n    <ng-container #fieldComponent></ng-container>\n  ",
        })
    ], FormlyWrapperLabel);
    return FormlyWrapperLabel;
}(field_wrapper_FieldWrapper));


// CONCATENATED MODULE: ./src/material/src/wrappers/description.ts



var description_FormlyWrapperDescription = (function (_super) {
    __extends(FormlyWrapperDescription, _super);
    function FormlyWrapperDescription() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyWrapperDescription.prototype, "fieldComponent", void 0);
    FormlyWrapperDescription = __decorate([
        Object(core_["Component"])({
            selector: 'formly-wrapper-description',
            template: "\n    <ng-container #fieldComponent></ng-container>\n    <div>\n      <small class=\"text-muted\">{{ to.description }}</small>\n    </div>\n  ",
        })
    ], FormlyWrapperDescription);
    return FormlyWrapperDescription;
}(field_wrapper_FieldWrapper));


// CONCATENATED MODULE: ./src/material/src/wrappers/message-validation.ts



var message_validation_FormlyWrapperValidationMessages = (function (_super) {
    __extends(FormlyWrapperValidationMessages, _super);
    function FormlyWrapperValidationMessages() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormlyWrapperValidationMessages.prototype, "validationId", {
        get: function () {
            return this.field.id + '-message';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyWrapperValidationMessages.prototype, "fieldComponent", void 0);
    FormlyWrapperValidationMessages = __decorate([
        Object(core_["Component"])({
            selector: 'formly-wrapper-validation-messages',
            template: "\n    <ng-container #fieldComponent></ng-container>\n    <div>\n      <small class=\"text-muted text-danger\" *ngIf=\"valid\" role=\"alert\" [id]=\"validationId\"><formly-validation-message [fieldForm]=\"formControl\" [field]=\"field\"></formly-validation-message></small>\n    </div>\n  ",
        })
    ], FormlyWrapperValidationMessages);
    return FormlyWrapperValidationMessages;
}(field_wrapper_FieldWrapper));


// CONCATENATED MODULE: ./src/material/src/wrappers/wrappers.ts





// CONCATENATED MODULE: ./core.ts


// CONCATENATED MODULE: ./src/material/src/formly.validation-message.ts




var formly_validation_message_FormlyValidationMessage = (function () {
    function FormlyValidationMessage(formlyMessages) {
        this.formlyMessages = formlyMessages;
    }
    Object.defineProperty(FormlyValidationMessage.prototype, "errorMessage", {
        get: function () {
            for (var error in this.fieldForm.errors) {
                if (this.fieldForm.errors.hasOwnProperty(error)) {
                    var message = this.formlyMessages.getValidatorErrorMessage(error);
                    if (this.field.validation && this.field.validation.messages && this.field.validation.messages[error]) {
                        message = this.field.validation.messages[error];
                    }
                    if (this.field.validators && this.field.validators[error] && this.field.validators[error].message) {
                        message = this.field.validators[error].message;
                    }
                    if (this.field.asyncValidators && this.field.asyncValidators[error] && this.field.asyncValidators[error].message) {
                        message = this.field.asyncValidators[error].message;
                    }
                    if (typeof message === 'function') {
                        return message(this.fieldForm.errors[error], this.field);
                    }
                    return message;
                }
                else {
                    return '';
                }
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", forms_["FormControl"])
    ], FormlyValidationMessage.prototype, "fieldForm", void 0);
    __decorate([
        Object(core_["Input"])(),
        __metadata("design:type", Object)
    ], FormlyValidationMessage.prototype, "field", void 0);
    FormlyValidationMessage = __decorate([
        Object(core_["Component"])({
            selector: 'formly-validation-message',
            template: "{{ errorMessage }}",
        }),
        __metadata("design:paramtypes", [formly_validation_messages_FormlyValidationMessages])
    ], FormlyValidationMessage);
    return FormlyValidationMessage;
}());


// EXTERNAL MODULE: external "@angular/material"
var material_ = __webpack_require__(7);
var material__default = /*#__PURE__*/__webpack_require__.n(material_);

// CONCATENATED MODULE: ./src/material/src/wrappers/addons.ts



var addons_FormlyWrapperAddons = (function (_super) {
    __extends(FormlyWrapperAddons, _super);
    function FormlyWrapperAddons() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyWrapperAddons.prototype.addonRightClick = function ($event) {
        if (this.to.addonRight.onClick) {
            this.to.addonRight.onClick(this.to, this, $event);
        }
    };
    FormlyWrapperAddons.prototype.addonLeftClick = function ($event) {
        if (this.to.addonLeft.onClick) {
            this.to.addonLeft.onClick(this.to, this, $event);
        }
    };
    __decorate([
        Object(core_["ViewChild"])('fieldComponent', { read: core_["ViewContainerRef"] }),
        __metadata("design:type", core_["ViewContainerRef"])
    ], FormlyWrapperAddons.prototype, "fieldComponent", void 0);
    FormlyWrapperAddons = __decorate([
        Object(core_["Component"])({
            selector: 'formly-wrapper-addons',
            template: "\n    <div class=\"input-group\">\n      <div class=\"input-group-addon\"\n        *ngIf=\"to.addonLeft\"\n        [ngStyle]=\"{cursor: to.addonLeft.onClick ? 'pointer' : 'inherit'}\"\n        (click)=\"addonLeftClick($event)\">\n        <i [ngClass]=\"to.addonLeft.class\" *ngIf=\"to.addonLeft.class\"></i>\n        <span *ngIf=\"to.addonLeft.text\">{{ to.addonLeft.text }}</span>\n      </div>\n      <ng-container #fieldComponent></ng-container>\n      <div class=\"input-group-addon\"\n        *ngIf=\"to.addonRight\"\n        [ngStyle]=\"{cursor: to.addonRight.onClick ? 'pointer' : 'inherit'}\"\n        (click)=\"addonRightClick($event)\">\n        <i [ngClass]=\"to.addonRight.class\" *ngIf=\"to.addonRight.class\"></i>\n        <span *ngIf=\"to.addonRight.text\">{{ to.addonRight.text }}</span>\n      </div>\n    </div>\n  ",
        })
    ], FormlyWrapperAddons);
    return FormlyWrapperAddons;
}(field_wrapper_FieldWrapper));


// CONCATENATED MODULE: ./src/material/src/run/description.ts
var TemplateDescription = (function () {
    function TemplateDescription() {
    }
    TemplateDescription.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) {
            if (field && field.templateOptions && field.templateOptions.description) {
                return 'description';
            }
            return '';
        });
    };
    return TemplateDescription;
}());


// CONCATENATED MODULE: ./src/material/src/run/validation.ts
var TemplateValidation = (function () {
    function TemplateValidation() {
    }
    TemplateValidation.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) {
            if (field && field.validators) {
                return 'validation-message';
            }
            return '';
        });
    };
    return TemplateValidation;
}());


// CONCATENATED MODULE: ./src/material/src/run/addon.ts
var TemplateAddons = (function () {
    function TemplateAddons() {
    }
    TemplateAddons.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) {
            if (field && field.templateOptions && (field.templateOptions.addonLeft || field.templateOptions.addonRight)) {
                return 'addons';
            }
            return '';
        });
    };
    return TemplateAddons;
}());


// CONCATENATED MODULE: ./src/material/src/material.config.ts






var FIELD_TYPE_COMPONENTS = [
    input_FormlyFieldInput,
    checkbox_FormlyFieldCheckbox,
    radio_FormlyFieldRadio,
    multicheckbox_FormlyFieldMultiCheckbox,
    label_FormlyWrapperLabel,
    description_FormlyWrapperDescription,
    message_validation_FormlyWrapperValidationMessages,
    fieldset_FormlyWrapperFieldset,
    addons_FormlyWrapperAddons,
];
var MATERIAL_FORMLY_CONFIG = {
    types: [
        {
            name: 'input',
            component: input_FormlyFieldInput,
            wrappers: ['fieldset', 'label'],
        },
        {
            name: 'checkbox',
            component: checkbox_FormlyFieldCheckbox,
            wrappers: ['fieldset'],
        },
        {
            name: 'radio',
            component: radio_FormlyFieldRadio,
            wrappers: ['fieldset', 'label'],
        },
        {
            name: 'multicheckbox',
            component: multicheckbox_FormlyFieldMultiCheckbox,
            wrappers: ['fieldset', 'label'],
        },
    ],
    wrappers: [
        { name: 'label', component: label_FormlyWrapperLabel },
        { name: 'description', component: description_FormlyWrapperDescription },
        { name: 'validation-message', component: message_validation_FormlyWrapperValidationMessages },
        { name: 'fieldset', component: fieldset_FormlyWrapperFieldset },
        { name: 'addons', component: addons_FormlyWrapperAddons },
    ],
    manipulators: [
        { class: TemplateDescription, method: 'run' },
        { class: TemplateValidation, method: 'run' },
        { class: TemplateAddons, method: 'run' },
    ],
};

// CONCATENATED MODULE: ./src/material/src/material.module.ts








var material_module_FormlyMaterialModule = (function () {
    function FormlyMaterialModule() {
    }
    FormlyMaterialModule = __decorate([
        Object(core_["NgModule"])({
            declarations: FIELD_TYPE_COMPONENTS.concat([formly_validation_message_FormlyValidationMessage]),
            imports: [
                common_["CommonModule"],
                forms_["ReactiveFormsModule"],
                material_["MaterialModule"],
                core_module_FormlyModule.forRoot(MATERIAL_FORMLY_CONFIG),
            ],
        })
    ], FormlyMaterialModule);
    return FormlyMaterialModule;
}());


// CONCATENATED MODULE: ./src/material/src/material.ts





// CONCATENATED MODULE: ./src/material/public_api.ts


// CONCATENATED MODULE: ./src/material/index.ts


// CONCATENATED MODULE: ./src/index.ts
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyForm", function() { return formly_form_FormlyForm; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyField", function() { return formly_field_FormlyField; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyAttributes", function() { return formly_attributes_FormlyAttributes; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyConfig", function() { return formly_config_FormlyConfig; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyFormBuilder", function() { return formly_form_builder_FormlyFormBuilder; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyValidationMessages", function() { return formly_validation_messages_FormlyValidationMessages; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyPubSub", function() { return FormlyPubSub; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyEventEmitter", function() { return formly_event_emitter_FormlyEventEmitter; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "Field", function() { return field_Field; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FieldType", function() { return field_type_FieldType; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FieldWrapper", function() { return field_wrapper_FieldWrapper; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyModule", function() { return core_module_FormlyModule; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyValidationMessage", function() { return formly_validation_message_FormlyValidationMessage; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyMaterialModule", function() { return material_module_FormlyMaterialModule; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyFieldCheckbox", function() { return checkbox_FormlyFieldCheckbox; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyFieldMultiCheckbox", function() { return multicheckbox_FormlyFieldMultiCheckbox; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyFieldInput", function() { return input_FormlyFieldInput; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyFieldRadio", function() { return radio_FormlyFieldRadio; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyWrapperFieldset", function() { return fieldset_FormlyWrapperFieldset; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyWrapperLabel", function() { return label_FormlyWrapperLabel; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyWrapperDescription", function() { return description_FormlyWrapperDescription; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "FormlyWrapperValidationMessages", function() { return message_validation_FormlyWrapperValidationMessages; });




/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_7__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=ng-formly.umd.js.map