import { Component, Renderer, ViewChildren, QueryList, AfterContentChecked } from "@angular/core";
import { AbstractControl, FormControl, FormBuilder } from "@angular/forms";
import {
  Field, FieldType, FormlyPubSub, FormlyEventEmitter, FormlyFieldConfig, FormlyValidationMessages,
  // FormlyValueChangeEvent
} from "../../../core/src/core";
//import {SingleFocusDispatcher} from "ng-formly/lib/templates"
//import {MdRadio} from '@angular/material';


@Component({
  selector: "formly-field-radio",
  template: `
  <div [formGroup]="form">
  <label for="">{{to.label}}</label>
  <md-radio-group [(ngModel)]="model" [formControlName]="key">
    <p *ngFor="let option of to.options">
      <md-radio-button [value]="option.key" 
        (change)="inputChange($event, option.key)" (focus)="onInputFocus()"
        [disabled]="to.disabled" #inputElement>{{option.value}}
      </md-radio-button>
    </p>
  </md-radio-group>
</div>
    `,
  queries: { inputComponent: new ViewChildren("inputElement") }
})
export class FormlyFieldRadio extends FieldType {
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormControl(
      { value: model ? 'on' : undefined, disabled: field.templateOptions.disabled },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined,
    );
  }
}

/*
export class FormlyFieldCheckbox extends Field {

  constructor(fm: FormlyValidationMessages, ps: FormlyPubSub, private formBuilder: FormBuilder, renderer: Renderer,
              //focusDispatcher: SingleFocusDispatcher
            ) {
              super()
    //super(fm, ps, renderer, focusDispatcher);
  }

  inputComponent: QueryList<MdCheckbox>;

  inputChange(e: any, val: any): void {
    this.model = e.checked;
    //this.changeFn.emit(new FormlyValueChangeEvent(this.key, e.checked));
    //this.ps.setUpdated(true);
  }

  createControl(): AbstractControl {
    return this._control = this.formBuilder.control(this.model ? "on" : undefined);
  }

  protected setNativeFocusProperty(newFocusValue: boolean): void {
    if (this.inputComponent.length > 0) {
      // this.inputComponent.first.focus();
    }
  }
}*/
