import {Component, Renderer, ViewChildren, QueryList, AfterContentChecked} from "@angular/core";
import {AbstractControl, FormControl,FormBuilder} from "@angular/forms";
import {Field, FieldType,FormlyPubSub, FormlyEventEmitter, FormlyFieldConfig, FormlyValidationMessages, 
 // FormlyValueChangeEvent
} from "../../../core/src/core";
//import {SingleFocusDispatcher} from "ng-formly/lib/templates"
//import {MdCheckbox} from '@angular/material';


@Component({ 
  selector: "formly-field-multicheckbox",
  template: `
  <div [formGroup]="form">
  <div [formGroupName]="key" class="form-group">
      <label class="form-control-label" for="">{{to.label}}</label>
      <div *ngFor="let option of to.options">
          <label class="c-input c-radio">
              <md-checkbox type="checkbox" [formControlName]="option.key"
                ng-model="model[option.key]" (change)="ngOnChanges($event, option.key)"
                (focus)="onInputFocus()" [disabled]="to.disabled" #inputElement>{{option.value}}
              </md-checkbox>
              <span class="c-indicator"></span>
          </label>
      </div>
  <!--    <small class="text-muted">{{to.description}}</small> -->
  </div>
</div>
`,
  queries: {inputComponent: new ViewChildren("inputElement")}
})
export class FormlyFieldMultiCheckbox extends FieldType {
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormControl(
      { value: model ? 'on' : undefined, disabled: field.templateOptions.disabled },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined,
    );
  }
}

