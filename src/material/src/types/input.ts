import { Component, Renderer, ViewChildren, QueryList, AfterContentChecked } from "@angular/core";
import { AbstractControl, FormControl, FormBuilder } from "@angular/forms";
import {
  Field, FieldType, FormlyPubSub, FormlyEventEmitter, FormlyFieldConfig, FormlyValidationMessages,
  // FormlyValueChangeEvent
} from "../../../core/src/core";
//import {SingleFocusDispatcher} from "ng-formly/lib/templates"


@Component({
  selector: "formly-field-checkbox",
  template: `
  <div class="formly-md-input-wrapper" [formGroup]="form" [ngClass]="{'has-danger': !formControl.valid}" *ngIf="!to.hidden">
  <md-input-container>
  <input mdInput [type]="to.type" [formControlName]="key" class="form-control md-input-full-width" id="{{key}}"
    [disabled]="to.disabled"
    (keyup)="inputChange($event, 'value')" (change)="inputChange($event, 'value')" [(ngModel)]="model"
    (focus)="onInputFocus()" [ngClass]="{'form-control-danger': !form.controls[key].valid}" #inputElement>
    <md-placeholder [ngClass]="{'md-placeholder-error': !formControl.valid}">{{to.label}}</md-placeholder>
    </md-input-container>
  <small class="md-hint">{{to.description}}</small>
</div>
`,
  queries: { inputComponent: new ViewChildren("inputElement") }
})
export class FormlyFieldInput extends FieldType {
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormControl(
      { value: model ? 'on' : undefined, disabled: field.templateOptions.disabled },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined,
    );
  }
}

/*


import {Component, ElementRef, AfterViewInit, Renderer, ViewChildren, QueryList} from "@angular/core";
import {AbstractControl, FormControl,FormBuilder} from "@angular/forms";
import {Field, FieldType,FormlyPubSub, FormlyEventEmitter, FormlyFieldConfig, FormlyValidationMessages, 
import {MdInput} from '@angular/material';
import {MdCheckbox} from '@angular/material';

@Component({
  selector: "formly-field-input",
  template: `
      <div class="formly-md-input-wrapper" [formGroup]="form" [ngClass]="{'has-danger': !formControl.valid}" *ngIf="!to.hidden">
        <md-input [type]="to.type" [formControlName]="key" class="form-control md-input-full-width" id="{{key}}"
          [disabled]="to.disabled"
          (keyup)="inputChange($event, 'value')" (change)="inputChange($event, 'value')" [(ngModel)]="model"
          (focus)="onInputFocus()" [ngClass]="{'form-control-danger': !form.controls[key].valid}" #inputElement>
          <md-placeholder [ngClass]="{'md-placeholder-error': !formControl.valid}">{{to.label}}</md-placeholder>
        </md-input>
        <small class="md-hint">{{to.description}}</small>
        <small class="md-hint-error"><formly-message [control]="key" [formDir]="form"></formly-message></small>
      </div>
    `,
  queries: {inputComponent: new ViewChildren("inputElement")}
})
export class FormlyFieldInput extends Field implements AfterViewInit {

  constructor(fm: FormlyMessages, ps: FormlyPubSub, renderer: Renderer, focusDispatcher: SingleFocusDispatcher) {
    super(fm, ps, renderer, focusDispatcher);
  }

  inputComponent: QueryList<MdInput>;

  protected setNativeFocusProperty(newFocusValue: boolean): void {
    if (this.inputComponent.length > 0) {
      this.inputComponent.first.focus();
    }
  }
} */
