import {Component, Renderer, ViewChildren, QueryList, AfterContentChecked} from "@angular/core";
import {AbstractControl, FormControl,FormBuilder} from "@angular/forms";
import {Field, FieldType,FormlyPubSub, FormlyEventEmitter, FormlyFieldConfig, FormlyValidationMessages, 
 // FormlyValueChangeEvent
} from "../../../core/src/core";
//import {SingleFocusDispatcher} from "ng-formly/lib/templates"
import {MdCheckbox} from '@angular/material';


@Component({ 
  selector: "formly-field-checkbox",
  template: `
  <div class="form-group">
  <div [formGroup]="form">
    <label class="c-input c-checkbox">
      <md-checkbox (change)="ngOnChanges($event)" [formControlName]="key" ng-model="model"
      [disabled]="to.disabled" (focus)="onInputFocus()" #inputElement> {{to.label}}
      </md-checkbox>
      </label>
  </div>
`,
  queries: {inputComponent: new ViewChildren("inputElement")}
})
export class FormlyFieldCheckbox extends FieldType {
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormControl(
      { value: model ? 'on' : undefined, disabled: field.templateOptions.disabled },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined,
    );
  }
}

/*

    <div class="form-group">
      <div [formGroup]="form">
        <label class="c-input c-checkbox">
          <md-checkbox (change)="inputChange($event)" [formControlName]="key" ng-model="model"
          [disabled]="to.disabled" (focus)="onInputFocus()" #inputElement> {{to.label}}
          </md-checkbox>
          </label>
      </div>
    <!--  <small class="text-muted">{{to.description}}</small> -->

export class FormlyFieldCheckbox extends Field {

  constructor(fm: FormlyValidationMessages, ps: FormlyPubSub, private formBuilder: FormBuilder, renderer: Renderer,
              //focusDispatcher: SingleFocusDispatcher
            ) {
              super()
    //super(fm, ps, renderer, focusDispatcher);
  }

  inputComponent: QueryList<MdCheckbox>;

  inputChange(e: any, val: any): void {
    this.model = e.checked;
    //this.changeFn.emit(new FormlyValueChangeEvent(this.key, e.checked));
    //this.ps.setUpdated(true);
  }

  createControl(): AbstractControl {
    return this._control = this.formBuilder.control(this.model ? "on" : undefined);
  }

  protected setNativeFocusProperty(newFocusValue: boolean): void {
    if (this.inputComponent.length > 0) {
      // this.inputComponent.first.focus();
    }
  }
}*/
