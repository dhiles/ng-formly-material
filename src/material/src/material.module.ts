import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '../../../core';
import { MaterialModule } from '@angular/material';
import { MATERIAL_FORMLY_CONFIG, FIELD_TYPE_COMPONENTS } from './material.config';
import { FormlyValidationMessage } from './formly.validation-message';

@NgModule({
  declarations: [...FIELD_TYPE_COMPONENTS, FormlyValidationMessage],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    FormlyModule.forRoot(MATERIAL_FORMLY_CONFIG),
  ],
})
export class FormlyMaterialModule {
}
